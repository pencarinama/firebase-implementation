﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    private static GameManager _instance = null;
    public static GameManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    [Range(0f, 1f)]
    public float AutoCollectPercentage = .1f;
    public ResourceConfig[] ResourceConfigs;

    public Transform ResourcesParent;
    public ResourceController ResourcePrefab;

    public Text GoldInfo;
    public Text AutoCollectInfo;

    private List<ResourceController> _activeResources = new List<ResourceController>();

    private float _collectSecond;
    //public double TotalGold { get; private set; }

    public TapText TapTextPrefab;
    public Transform CoinIcon;
    private List<TapText> _tapTextPool = new List<TapText>();

    public Sprite[] ResourcesSprite;
    public float saveDelay = 5f;

    private float _saveDelayCounter;

    // Start is called before the first frame update
    void Start()
    {
        AddAllResources();
        GoldInfo.text = $"Gold: {UserDataManager.Progress.Gold.ToString("0")}";
    }

    // Update is called once per frame
    void Update()
    {
        float deltaTime = Time.unscaledDeltaTime;
        _saveDelayCounter -= deltaTime;

        _collectSecond += deltaTime;
        if (_collectSecond >= 1f)
        {
            CollectPerSecond();
            _collectSecond = 0f;
        }

        CheckResourceCost();

        CoinIcon.transform.localScale = Vector3.LerpUnclamped(CoinIcon.transform.localScale, Vector3.one * 2f, 0.15f);
        CoinIcon.transform.Rotate(0f, 0f, Time.deltaTime * -100f);
    }

   private void AddAllResources()
    {
        int index = 0;
        bool showResource = true;
        foreach(ResourceConfig config in ResourceConfigs)
        {
            GameObject obj = Instantiate(ResourcePrefab.gameObject, ResourcesParent, false);
            ResourceController resource = obj.GetComponent<ResourceController>();
            resource.SetConfig(index, config);
            _activeResources.Add(resource);

            obj.gameObject.SetActive(showResource);
            if(showResource && !resource.IsUnlocked)
            {
                showResource = false;
            }
            index++;
        }
    }

    private void CollectPerSecond()
    {
        double Output = 0;
        foreach(ResourceController resource in _activeResources)
        {
            if(resource.IsUnlocked)
            {
               Output += resource.GetOutput();
            }
        }

        Output *= AutoCollectPercentage;

        AutoCollectInfo.text = $"Auto Collect: { Output.ToString("F1")} / second";
        AddGold(Output);
    }

    public void AddGold(double value)
    {
        UserDataManager.Progress.Gold += value;
        GoldInfo.text = $"Gold: { UserDataManager.Progress.Gold.ToString("0")}";

        UserDataManager.Save();

        AchievementController.Instance.UnlockAchievement(AchievementType.TotalGold, UserDataManager.Progress.Gold.ToString("0"));

        UserDataManager.Save(_saveDelayCounter < 0f);
        if (_saveDelayCounter < 0f)
        {
            _saveDelayCounter = saveDelay;
        }
    }

    public void CollectByTap(Vector3 tapPosition, Transform parent)
    {
        double Output = 0;

        foreach(ResourceController resource in _activeResources)
        {
            if(resource.IsUnlocked)
            {
                Output += resource.GetOutput();
            }
        }

        TapText tapText = GetOrCreateTapText();
        tapText.transform.SetParent(parent, false);
        tapText.transform.position = tapPosition;

        tapText.text.text = $"+{Output.ToString("0")}";

        tapText.gameObject.SetActive(true);
        CoinIcon.transform.localScale = Vector3.one * 1.75f;
        AddGold(Output);
    }

    private TapText GetOrCreateTapText()
    {
        TapText tapText = _tapTextPool.Find(t => !t.gameObject.activeSelf);
        if(tapText == null)
        {
            tapText = Instantiate(TapTextPrefab).GetComponent<TapText>();
            _tapTextPool.Add(tapText);
        }

        return tapText;
    }

    private void CheckResourceCost()
    {
        foreach(ResourceController resource in _activeResources)
        {
            bool isBuyable = false;
            if (resource.IsUnlocked)
            {
                isBuyable = UserDataManager.Progress.Gold >= resource.GetUpgradeCost();
            }
            else
            {
                isBuyable = UserDataManager.Progress.Gold >= resource.GetUnlockCost();
            }
            resource.ResourceImage.sprite = ResourcesSprite[isBuyable ? 1 : 0];
        }
    }

    public void ShowNextResource()
    {
        foreach(ResourceController resource in _activeResources)
        {
            if(!resource.gameObject.activeSelf)
            {
                resource.gameObject.SetActive(true);
                break;
            }
        }
    }
}

[System.Serializable]
public struct ResourceConfig
{
    public string Name;
    public double UnlockCost;
    public double UpgradeCost;
    public double Output;
}