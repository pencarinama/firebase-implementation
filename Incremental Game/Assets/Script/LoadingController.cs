﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LoadingController : MonoBehaviour
{

    [SerializeField] private Button _localButton;
    [SerializeField] private Button _cloudButton;
    // Start is called before the first frame update
    void Start()
    {

        _localButton.onClick.AddListener(() =>
        {
            SetButtonInteractible(false);
            UserDataManager.LoadFromLocal();
            SceneManager.LoadScene(1);
        });

        _cloudButton.onClick.AddListener(() =>
        {
            SetButtonInteractible(false);
            StartCoroutine(UserDataManager.LoadFromCloud(() =>
            {
                SceneManager.LoadScene(1);
            }));
        });
    }

    private void SetButtonInteractible(bool interactible)
    {
        _localButton.interactable = interactible;
        _cloudButton.interactable = interactible; 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
