﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Firebase.Storage;
using System.Threading.Tasks;
using System.Text;

public class UserDataManager : MonoBehaviour
{
    private const string PROGRESS_KEY = "Progress";

    public static UserProgressData Progress;

    public static void LoadFromLocal()
    {
        if(!PlayerPrefs.HasKey(PROGRESS_KEY))
        {
            Progress = new UserProgressData();
            Save(true);
        }
        else
        {
            string json = PlayerPrefs.GetString(PROGRESS_KEY);
            Progress = JsonUtility.FromJson<UserProgressData>(json);
        }
    }

    public static void Save(bool uploadToCloud = false)
    {
        string json = JsonUtility.ToJson(Progress);
        PlayerPrefs.SetString(PROGRESS_KEY, json);

        if(uploadToCloud)
        {

            AnalyticsManager.SetUserProperties("gold", Progress.Gold.ToString());
            byte[] data = Encoding.Default.GetBytes(json);
            StorageReference targetStorage = GetTargetCloudStorage();
            targetStorage.PutBytesAsync(data);
        }
    }

    public static bool HasResources(int index)
    {
        if(Progress.ResourcesLevels.Count > index)
        {
            return true;
        }
        return false;
    }

    public static IEnumerator LoadFromCloud(System.Action OnComplete)
    {
        StorageReference targetStorage = GetTargetCloudStorage();

        bool isComplete = false;
        bool isSuccess = false;
        const long maxAllowedSize = 1024 * 1024;
        targetStorage.GetBytesAsync(maxAllowedSize).ContinueWith((Task<byte[]> task) =>
        {
            if(!task.IsFaulted)
            {
                string json = Encoding.Default.GetString(task.Result);
                Progress = JsonUtility.FromJson<UserProgressData>(json);
                isSuccess = true;
            }
            isComplete = true;
        });

        while(!isComplete)
        {
            yield return null;
        }

        if(isSuccess)
        {
            Save();
        }
        else
        {
            LoadFromLocal();
        }

        OnComplete?.Invoke();

    }

    public static StorageReference GetTargetCloudStorage()
    {
        string deviceID = SystemInfo.deviceUniqueIdentifier;
        FirebaseStorage storage = FirebaseStorage.DefaultInstance;

        return storage.GetReferenceFromUrl($"{storage.RootReference}/{deviceID}");
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
